﻿using GeneralaBLL;
using GeneralaCommon;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaController
{
    public class PartidaController
    {

        PartidaService partidaService = new PartidaService();
        public void iniciarPartida(PartidaDTO partida)
        {
            partidaService.iniciarPartida(partida);
        }

        public void terminarPartida(PartidaDTO partida)
        {
            partidaService.terminarPartida(partida);
        }

        public EstadisticasDTO getStatistics(UsuarioDTO jugador)
        {
            return partidaService.getStatistics(jugador);
        }
    }
}
