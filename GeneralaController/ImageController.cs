﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GeneralaController
{
    public class ImageController
    {
        public static Image getImage(string img)
        {
            return Image.FromFile(img);
        }
    }
}
