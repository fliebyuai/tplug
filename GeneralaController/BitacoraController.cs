﻿using GeneralaBLL;
using GeneralaCommon;
using GeneralaModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaController
{
    public class BitacoraController
    {
        BitacoraService bitacoraService = new BitacoraService();

        public List<BitacoraDTO> getUserBitacora(UsuarioDTO usuario)
        {
            if (usuario == null) throw new Exception("Error al cargar usuario");
            return bitacoraService.getUserBitacora(usuario);
        }
    }
}
