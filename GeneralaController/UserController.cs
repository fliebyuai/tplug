﻿using GeneralaBLL;
using GeneralaCommon;
using GeneralaModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaController
{
    public class UserController
    {

        private UserServiice userService = new UserServiice();

        public List<Usuario> findAll()
        {
            return userService.findAll();
        }

        public void CreateUser(string user, string pass, string name, string last)
        {
            if (user == null || user.Equals(""))
            {
                throw new Exception("El Usuario no puede estar vacio");
            }
            if (pass == null || pass.Equals(""))
            {
                throw new Exception("El Password no puede estar vacio");
            }
            if (name == null || name.Equals(""))
            {
                throw new Exception("El Nombre no puede estar vacio");
            }
            if (last == null || last.Equals(""))
            {
                throw new Exception("El Apellido no puede estar vacio");
            }
            try
            {
                userService.CreateUser(user, pass, name, last);
            }
            catch (Exception error)
            {
                throw error;
            }
        }

        public UsuarioDTO login(string user, string pass)
        {
            if (user == null || user.Equals(""))
                throw new Exception("El usuario no puede estar vacio");
            if (pass == null || pass.Equals(""))
                throw new Exception("La clave no puede estar vacia");
            return userService.login(user, pass);
        }

        public void logout(UsuarioDTO usuario)
        {
            userService.logout(usuario);
        }
    }
}
