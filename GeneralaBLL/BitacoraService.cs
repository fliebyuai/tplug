﻿using GeneralaCommon;
using GeneralaDAL;
using GeneralaModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaBLL
{
    public class BitacoraService
    {
        BitacoraDAO bitacoraDAO = new BitacoraDAO();

        public List<BitacoraDTO> getUserBitacora(UsuarioDTO usuario)
        {
            List<BitacoraDTO> dto = new List<BitacoraDTO>();
            List<Bitacora> btc = bitacoraDAO.findByUser(usuario.IdUsuario);
            btc.ForEach(b => dto.Add(assembleBitacora(b)));
            return dto;
        }

        private BitacoraDTO assembleBitacora(Bitacora b)
        {
            BitacoraDTO dto = new BitacoraDTO();
            dto.Tipo = b.Tipo;
            dto.Fecha = b.Datetime;
            return dto;
        }
    }
}
