﻿using GeneralaCommon;
using GeneralaDAL;
using GeneralaModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml.Linq;

namespace GeneralaBLL
{
    public class PartidaService
    {
        UserServiice userService = new UserServiice();
        PartidaDAO partidaDAO = new PartidaDAO();
        string docpath = "partidas.xml";

        public void iniciarPartida(PartidaDTO partida)
        {
            Partida dbPartida = assemblePartida(partida);
            partidaDAO.Save(dbPartida);
            userService.saveBitacora(Constantes.INICIO_PARTIDA, dbPartida.Usuario1);
            userService.saveBitacora(Constantes.INICIO_PARTIDA, dbPartida.Usuario2);
        }

        public EstadisticasDTO getStatistics(UsuarioDTO jugador)
        {
            EstadisticasDTO dto = new EstadisticasDTO();
            dto.PartidasGanadas = partidaDAO.encontrarPartidasGanadas(jugador.IdUsuario.ToString());
            dto.PartidasPerdidas = partidaDAO.encontrarPartidasPerdidas(jugador.IdUsuario.ToString());
            dto.PartidasEmpatadas = partidaDAO.encontrarPartidasEmpatadas(jugador.IdUsuario.ToString());
            float preResultado = float.Parse(dto.PartidasGanadas.ToString()) / (dto.PartidasGanadas + dto.PartidasPerdidas + dto.PartidasEmpatadas);
            dto.PromedioVictorias = (int)(preResultado * 100);
            dto.TiempoDeJuego = partidaDAO.buscarTiempodeJuego(jugador.IdUsuario.ToString());
            return dto;
        }

        public void terminarPartida(PartidaDTO partida)
        {
            Partida dbPartida = assemblePartida(partida);
            dbPartida.FechaFin = DateTime.Now;
            partidaDAO.Save(dbPartida);
            userService.saveBitacora(Constantes.FIN_PARTIDA, dbPartida.Usuario1);
            userService.saveBitacora(Constantes.FIN_PARTIDA, dbPartida.Usuario2);

            XDocument doc = null;
            try
            {
                doc = XDocument.Load(docpath);
            }
            catch(Exception e)
            { 

                FileStream fileStream=null;
                try
                {
                    fileStream = File.Create(docpath);
                    string dataasstring = "<PARTIDAS></PARTIDAS>"; 
                    byte[] info = new UTF8Encoding(true).GetBytes(dataasstring);
                    fileStream.Write(info, 0, info.Length);
                }
                finally
                {
                    fileStream.Close();
                }
                
                doc = XDocument.Load(docpath);
            }
           
            
            XElement partidas = doc.Element("PARTIDAS");

            if(partidas == null)
            {
                XElement rootPartidas = new XElement("PARTIDAS");
                doc.Save(docpath);
            } 

            XElement Partida = new XElement("PARTIDA", new object[] {
                new XElement("FECHA_INICIO", partida.FechaInicio),
                new XElement("FECHA_FIN", partida.FechaFin),
                new XElement("JUGADOR_1", partida.Jugador1.IdUsuario),
                new XElement("JUGADOR_2", partida.Jugador2.IdUsuario),
                new XElement("PUNTAJE_JUGADOR_1", new object[] {
                    new XElement("UNO", partida.PJugador1.One),
                    new XElement("DOS", partida.PJugador1.Two),
                    new XElement("TRES", partida.PJugador1.Three),
                    new XElement("CUATRO", partida.PJugador1.Four),
                    new XElement("CICO", partida.PJugador1.Five),
                    new XElement("SEIS", partida.PJugador1.Six),
                    new XElement("ESCALERA", partida.PJugador1.Stair),
                    new XElement("FULL", partida.PJugador1.Full),
                    new XElement("POKER", partida.PJugador1.Poker),
                    new XElement("GENERALA", partida.PJugador1.Generala),
                    new XElement("DOBLE_GENERALA", partida.PJugador1.DoubleGenerala),
                    new XElement("TOTAL", partida.PJugador1.getTotal())
                }),
                new XElement("PUNTAJE_JUGADOR_2", new object[] {
                    new XElement("UNO", partida.PJugador2.One),
                    new XElement("DOS", partida.PJugador2.Two),
                    new XElement("TRES", partida.PJugador2.Three),
                    new XElement("CUATRO", partida.PJugador2.Four),
                    new XElement("CICO", partida.PJugador2.Five),
                    new XElement("SEIS", partida.PJugador2.Six),
                    new XElement("ESCALERA", partida.PJugador2.Stair),
                    new XElement("FULL", partida.PJugador2.Full),
                    new XElement("POKER", partida.PJugador2.Poker),
                    new XElement("GENERALA", partida.PJugador2.Generala),
                    new XElement("DOBLE_GENERALA", partida.PJugador2.DoubleGenerala),
                    new XElement("TOTAL", partida.PJugador2.getTotal())
                })
            });

            doc.Element("PARTIDAS").Add(Partida);

            doc.Save(docpath);
        }

        private Partida assemblePartida(PartidaDTO partida)
        {
            Partida p = new Partida();
            p.FechaInicio = partida.FechaInicio;
            p.FechaFin = partida.FechaFin;
            p.Usuario1 = userService.assembleUser(partida.Jugador1);
            p.Usuario2 = userService.assembleUser(partida.Jugador2);
            p.PuntajeUsuario1 = partida.getPuntaje(partida.Jugador1);
            p.PuntajeUsuario2 = partida.getPuntaje(partida.Jugador2);
            return p;
        }
    }
}
