﻿using GeneralaCommon;
using GeneralaDAL;
using GeneralaModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaBLL
{
    public class UserServiice
    {
        private UsuarioDAO usuarioDAO = new UsuarioDAO();
        private BitacoraDAO bitacoraDAO = new BitacoraDAO();

        public List<GeneralaModel.Usuario> findAll()
        {
            return usuarioDAO.FindAll();
        }

        public void CreateUser(string user, string pass, string name, string last)
        {
            GeneralaModel.Usuario u = new GeneralaModel.Usuario();
            u.NombreUsuario = user;
            u.Clave = HashUtil.HashPassword(pass);
            u.Nombre = name;
            u.Apellido = last;
            try
            {
                int save = usuarioDAO.Save(u);
                if (save == -1) throw new Exception("ERROR Al insertar nuevo usuario");
            }
            catch (Exception error)
            {
                throw error;
            }
        }

        public UsuarioDTO login(string user, string pass)
        {
            Usuario usuario = usuarioDAO.findByUsername(user);
            if(HashUtil.VerifyHashedPassword(usuario.Clave, pass))
            {
                saveBitacora(Constantes.INICIO_SESION, usuario);
                usuario.Clave = null;
                return assembleUser(usuario);
            }
            throw new Exception("La clave ingresada es invalida");
        }

        public void saveBitacora(string type, Usuario usuario)
        {
            Bitacora bitacora = new Bitacora();
            bitacora.Usuario = usuario;
            bitacora.Tipo = type;
            bitacora.Datetime = DateTime.Now;
            bitacoraDAO.Save(bitacora);
        }
        private UsuarioDTO assembleUser(Usuario usuario)
        {
            UsuarioDTO u = new UsuarioDTO();
            u.IdUsuario = usuario.IdUsuario;
            u.NombreUsuario = usuario.NombreUsuario;
            u.Nombre = usuario.Nombre;
            u.Apellido = usuario.Apellido;
            u.Clave = usuario.Clave;
            return u;
        }

        public void logout(UsuarioDTO usuario)
        {
            saveBitacora(Constantes.FIN_SESION, assembleUser(usuario));
        }

        public Usuario assembleUser(UsuarioDTO usuario)
        {
            Usuario u = new Usuario();
            u.IdUsuario = usuario.IdUsuario;
            u.NombreUsuario = usuario.NombreUsuario;
            u.Nombre = usuario.Nombre;
            u.Apellido = usuario.Apellido;
            u.Clave = usuario.Clave;
            return u;
        }
    }
}
