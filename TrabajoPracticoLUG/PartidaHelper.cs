﻿using GeneralaCommon;
using GeneralaController;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TrabajoPracticoLUG;

namespace GeneralaViewLayer
{
    public class PartidaHelper
    {
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private PictureBox pictureBox3;
        private PictureBox pictureBox4;
        private PictureBox pictureBox5;
        private PictureBox pictureBox11;
        private PictureBox pictureBox12;
        private PictureBox pictureBox13;
        private PictureBox pictureBox14;
        private PictureBox pictureBox15;
        private Label informe1;
        private Label informe2;

        public PartidaHelper(PictureBox pictureBox1, PictureBox pictureBox2, PictureBox pictureBox3, PictureBox pictureBox4, PictureBox pictureBox5, PictureBox pictureBox11, PictureBox pictureBox12, PictureBox pictureBox13, PictureBox pictureBox14, PictureBox pictureBox15, Label informe1, Label informe2)
        {
            this.pictureBox1 = pictureBox1;
            this.pictureBox2 = pictureBox2;
            this.pictureBox3 = pictureBox3;
            this.pictureBox4 = pictureBox4;
            this.pictureBox5 = pictureBox5;
            this.pictureBox11 = pictureBox11;
            this.pictureBox12 = pictureBox12;
            this.pictureBox13 = pictureBox13;
            this.pictureBox14 = pictureBox14;
            this.pictureBox15 = pictureBox15;
            this.informe1 = informe1;
            this.informe2 = informe2;
        }

        public void clickPictureBox(PictureBox pBox, PictureBox sPbox, int dado, Label turno, TableLayoutPanel tablaPuntajes)
        {
            pBox.Hide();
            Form1.acumulador.Add(dado, Form1.tiro.GetValueOrDefault(dado));
            sPbox.Show();
            sPbox.Image = ImageController.getImage("../../../img/dado" + Form1.acumulador.GetValueOrDefault(dado) + ".jpg");
            validarJugada(turno, tablaPuntajes);
        }

        public void switchTurnoUser(Label turno)
        {
            //procesarPuntos();
            Form1.countTiros = 0;
            //ResetearDadosPequenios();
            if (Form1.idUserTurno.Equals(Form1.jugador1.IdUsuario))
            {
                Form1.idUserTurno = Form1.jugador2.IdUsuario;
                turno.Text = "Turno  de " + Form1.jugador2.Nombre;
                this.informe2.Text = "";
            }
            else
            {
                Form1.idUserTurno = Form1.jugador1.IdUsuario;
                turno.Text = "Turno de " + Form1.jugador1.Nombre;
                this.informe1.Text = "";

            }
        }

        public void DibujarPuntos(TableLayoutPanel tablaPuntajes)
        {

            DibujarPuntaje(tablaPuntajes, Form1.PartidaEnCurso.PJugador1, 1);
            DibujarPuntaje(tablaPuntajes, Form1.PartidaEnCurso.PJugador2, 2);
        }

        public void fillCell(TableLayoutPanel table, string text, int col, int row, Color color)
        {
            Label l = new Label();
            l.AutoSize = true;
            l.Text = text;

            if (color != null) l.ForeColor = color;
            if (table.GetControlFromPosition(col, row) != null)
            {
                table.GetControlFromPosition(col, row).Text = text;
                table.GetControlFromPosition(col, row).ForeColor = color;
            }
            else
            {
                table.Controls.Add(l, col, row);
            }
        }

        public void PreloadResultsTable(TableLayoutPanel tablaPuntajes)
        {
            fillCell(tablaPuntajes, "JUGADOR", 0, 0, Color.Blue);
            fillCell(tablaPuntajes, "1", 1, 0, Color.Blue);
            fillCell(tablaPuntajes, "2", 2, 0, Color.Blue);
            fillCell(tablaPuntajes, "3", 3, 0, Color.Blue);
            fillCell(tablaPuntajes, "4", 4, 0, Color.Blue);
            fillCell(tablaPuntajes, "5", 5, 0, Color.Blue);
            fillCell(tablaPuntajes, "6", 6, 0, Color.Blue);
            fillCell(tablaPuntajes, "E", 7, 0, Color.Blue);
            fillCell(tablaPuntajes, "F", 8, 0, Color.Blue);
            fillCell(tablaPuntajes, "P", 9, 0, Color.Blue);
            fillCell(tablaPuntajes, "G", 10, 0, Color.Blue);
            fillCell(tablaPuntajes, "DG", 11, 0, Color.Blue);
            fillCell(tablaPuntajes, "T", 12, 0, Color.Blue);
        }

        

        public void handleTurno(Label turno, bool reset = true)
        {
            if (reset)
            {
                Form1.acumulador = new Dictionary<int, int>();
                //tirar();
                //RenderDados();

            }
            switchTurnoUser(turno);
            ResetearDadosPequenios();
        }

        public void tirar()
        {
            if (Form1.countTiros == 0)
            {
                Form1.acumulador = new Dictionary<int, int>();
            }
            Form1.tiro = new Dictionary<int, int>();
            Dictionary<int, int> dados = TirarDados(!Form1.acumulador.ContainsKey(1), !Form1.acumulador.ContainsKey(2), !Form1.acumulador.ContainsKey(3), !Form1.acumulador.ContainsKey(4), !Form1.acumulador.ContainsKey(5));
            if (!Form1.acumulador.ContainsKey(1)) Form1.tiro.Add(1, dados.GetValueOrDefault(1));
            if (!Form1.acumulador.ContainsKey(2)) Form1.tiro.Add(2, dados.GetValueOrDefault(2));
            if (!Form1.acumulador.ContainsKey(3)) Form1.tiro.Add(3, dados.GetValueOrDefault(3));
            if (!Form1.acumulador.ContainsKey(4)) Form1.tiro.Add(4, dados.GetValueOrDefault(4));
            if (!Form1.acumulador.ContainsKey(5)) Form1.tiro.Add(5, dados.GetValueOrDefault(5));
        }


        public void CompletarNombresJugadores(TableLayoutPanel tablaPuntajes)
        {
            fillCell(tablaPuntajes, Form1.jugador1.NombreUsuario, 0, 1, Color.Black);
            fillCell(tablaPuntajes, Form1.jugador2.NombreUsuario, 0, 2, Color.Black);
        }

        public void RenderDados()
        {
            RenderDado(this.pictureBox1, 1);
            RenderDado(this.pictureBox2, 2);
            RenderDado(this.pictureBox3, 3);
            RenderDado(this.pictureBox4, 4);
            RenderDado(this.pictureBox5, 5);
        }

        private Dictionary<int, int> TirarDados(bool one, bool two, bool three, bool four, bool five)
        {
            Dictionary<int, int> dados = new Dictionary<int, int>();
            if (one) dados.Add(1, new Random().Next(1, 7));
            if (two) dados.Add(2, new Random().Next(1, 7));
            if (three) dados.Add(3, new Random().Next(1, 7));
            if (four) dados.Add(4, new Random().Next(1, 7));
            if (five) dados.Add(5, new Random().Next(1, 7));
            return dados;
        }

        private void RenderDado(PictureBox pBox, int dado)
        {
            if (!Form1.acumulador.ContainsKey(dado))
            {
                pBox.Image = ImageController.getImage("../../../img/dado" + Form1.tiro.GetValueOrDefault(dado) + ".jpg");
                pBox.Show();
            }
        }



        private void ResetearDadosPequenios()
        {
            this.pictureBox11.Hide();
            this.pictureBox12.Hide();
            this.pictureBox13.Hide();
            this.pictureBox14.Hide();
            this.pictureBox15.Hide();
        }
        

        public void validarJugada(Label turno, TableLayoutPanel tablaPuntajes)
        {
            if (Form1.acumulador.Count == 5 || Form1.countTiros > 3)
            {
                procesarPuntos(tablaPuntajes);
                //Cambiar de jugador y anotar los puntos
                handleTurno(turno);
            }
        }

        private void procesarPuntos(TableLayoutPanel tablaPuntajes)
        {
            bool _anotoAlgo = false;
            //PartidaEnCurso
            if (EsGenerala()) 
            {
                DibujarEInformar(tablaPuntajes, "GENERALA !!!");
                return;
            }

            if (EsPoker())
            {
                DibujarEInformar(tablaPuntajes, "POKER !!!");
                return;
            }

            if (EsFull())
            {
                DibujarEInformar(tablaPuntajes, "FULL !!!");
                return;
            }

            if (EsEscalera())
            {
                DibujarEInformar(tablaPuntajes, "ESCALERA !!!");
                return;
            }

            if(!EsGenerala()  && !EsPoker() && !EsFull() && !EsEscalera())
            {
                Dictionary<int, int> mejorOcurrencia = CalculaMejorOcurrencia();
                if (mejorOcurrencia.Count > 0)
                {
                    _anotoAlgo = true;
                    DibujarEInformar(tablaPuntajes, mejorOcurrencia.ElementAt(0).Value + " al " + mejorOcurrencia.ElementAt(0).Key + "!!!");
                }
                
            }

            bool fin = false;
            if (!_anotoAlgo)
            {
                fin = !tacharMayor();
                DibujarEInformar(tablaPuntajes, "No se anoto puntaje");
            }
            
            if(fin || FinDeJuego())
            {
                Puntaje ganador = Form1.PartidaEnCurso.PJugador1.getTotal() > Form1.PartidaEnCurso.PJugador2.getTotal() ? Form1.PartidaEnCurso.PJugador1 : Form1.PartidaEnCurso.PJugador1.getTotal() < Form1.PartidaEnCurso.PJugador2.getTotal()? Form1.PartidaEnCurso.PJugador2 : null;
                if (ganador != null)
                    MessageBox.Show("Felicitaciones jugador " + ganador.Jugador.Nombre + " " + ganador.Jugador.Apellido + " ha ganado el juego con " + ganador.getTotal() + " Puntos!!");
                else
                    MessageBox.Show("Empate!!");
                
            }

        }

        public bool FinDeJuego()
        {
            bool fin = true;
            if (Form1.PartidaEnCurso.PJugador1.One == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.Two == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.Three == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.Four == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.Five == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.Six == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.Stair == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.Full == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.Poker == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.Generala == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador1.DoubleGenerala == 0) fin = false;

            if (Form1.PartidaEnCurso.PJugador2.One == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.Two == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.Three == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.Four == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.Five == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.Six == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.Stair == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.Full == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.Poker == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.Generala == 0) fin = false;
            if (Form1.PartidaEnCurso.PJugador2.DoubleGenerala == 0) fin = false;

            return fin;
        }

        private bool tacharMayor()
        {
            Puntaje _p = Form1.PartidaEnCurso.Jugador1.IdUsuario.Equals(Form1.idUserTurno) ? Form1.PartidaEnCurso.PJugador1 : Form1.PartidaEnCurso.PJugador2;
            if (_p.DoubleGenerala == 0) _p.DoubleGenerala = -1;
            else if (_p.Generala == 0) _p.Generala = -1;
            else if (_p.Poker == 0) _p.Poker = -1;
            else if (_p.Full == 0) _p.Full = -1;
            else if (_p.Stair == 0) _p.Stair = -1;
            else if (_p.Six == 0) _p.Six = -1;
            else if (_p.Five == 0) _p.Five = -1;
            else if (_p.Four == 0) _p.Four = -1;
            else if (_p.Three == 0) _p.Three = -1;
            else if (_p.Two == 0) _p.Two = -1;
            else if (_p.One == 0) _p.One = -1;
            else return false;
            return true;
        }

        private Dictionary<int, int> CalculaMejorOcurrencia()
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            Puntaje puntaje = Form1.PartidaEnCurso.Jugador1.IdUsuario.Equals(Form1.idUserTurno) ? Form1.PartidaEnCurso.PJugador1 : Form1.PartidaEnCurso.PJugador2;

            int i = 0;

            Dictionary<int, List<int>> acum = Form1.acumulador.GroupBy(r => r.Value).ToDictionary(t => t.Key, t => t.Select(r => r.Key).ToList());
            var order = from entry in acum orderby entry.Value.Count descending select entry;
            List<KeyValuePair<int, List<int>>>.Enumerator e = order.ToList().GetEnumerator();
            //Dictionary<int, List<int>>.Enumerator e =  acum.GetEnumerator();
            
            while (e.MoveNext())
            {
                List<int> value = e.Current.Value;
                if (!ExisteEnPuntaje(e.Current.Key))
                {
                    result.Add(e.Current.Key, e.Current.Value.Count * e.Current.Key);
                    if (result.ElementAt(0).Key == 1) puntaje.One = e.Current.Value.Count * e.Current.Key;
                    if (result.ElementAt(0).Key == 2) puntaje.Two = e.Current.Value.Count * e.Current.Key;
                    if (result.ElementAt(0).Key == 3) puntaje.Three = e.Current.Value.Count * e.Current.Key;
                    if (result.ElementAt(0).Key == 4) puntaje.Four = e.Current.Value.Count * e.Current.Key;
                    if (result.ElementAt(0).Key == 5) puntaje.Five = e.Current.Value.Count * e.Current.Key;
                    if (result.ElementAt(0).Key == 6) puntaje.Six = e.Current.Value.Count * e.Current.Key;
                    return result;
                }
            }
            return result;
        }

        private bool ExisteEnPuntaje(int key)
        {
            Puntaje puntaje = Form1.PartidaEnCurso.Jugador1.IdUsuario.Equals(Form1.idUserTurno) ? Form1.PartidaEnCurso.PJugador1 : Form1.PartidaEnCurso.PJugador2;
            if (key == 6) return puntaje.Six > 0;
            if (key == 5) return puntaje.Five > 0;
            if (key == 4) return puntaje.Four > 0;
            if (key == 3) return puntaje.Three > 0;
            if (key == 2) return puntaje.Two > 0;
            if (key == 1) return puntaje.One > 0;
            return false;
        }

        private int CountOcurrences(int number)
        {
            return 0;
        }

        private int GetMaxOcurrece()
        {
            int i = 0;
            Dictionary<int, List<int>> acum = Form1.acumulador.GroupBy(r => r.Value).ToDictionary(t => i++, t => t.Select(r => r.Key).ToList());
            Dictionary<int, List<int>>.Enumerator e = acum.GetEnumerator();
            int max = 0;
            int num = 0;
            while (e.MoveNext())
            {
                List<int> value = e.Current.Value;
                if (value.Count > max)
                {
                    max = value.Count;
                    num = value.ToArray()[0];
                }
            }
            return num;
        }

        private void DibujarEInformar(TableLayoutPanel tablaPuntajes, string mensaje)
        {
            if (Form1.idUserTurno.Equals(Form1.jugador1.IdUsuario))
            {
                this.informe1.Text = mensaje;
            }
            else
            {
                this.informe2.Text = mensaje;
            }
            DibujarPuntos(tablaPuntajes);
        }

        private void DibujarPuntaje(TableLayoutPanel tablaPuntajes, Puntaje p, int user)
        {
            int col = 1;
            fillCell(tablaPuntajes, ConvertirAX(p.One), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.Two), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.Three), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.Four), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.Five), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.Six), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.Stair), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.Full), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.Poker), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.Generala), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.DoubleGenerala), col++, user, Color.Black);
            fillCell(tablaPuntajes, ConvertirAX(p.getTotal()), col++, user, Color.Black);
        }

        private string ConvertirAX(int num)
        {
            if (num < 0) return "X";
            else return num.ToString();
        }

        private bool EsEscalera()
        {
            int i = 0;
            Puntaje puntaje = Form1.PartidaEnCurso.Jugador1.IdUsuario.Equals(Form1.idUserTurno) ? Form1.PartidaEnCurso.PJugador1 : Form1.PartidaEnCurso.PJugador2;
            Dictionary<int, List<int>> acum = Form1.acumulador.GroupBy(r => r.Value).ToDictionary(t => i++, t => t.Select(r => r.Key).ToList());
            if (acum.Count == 5)
            {
                if (puntaje.Stair == 0)
                    puntaje.Stair = 20;
                return true;
            }
            return false;
        }
        private bool EsPoker()
        {
            Puntaje puntaje = Form1.PartidaEnCurso.Jugador1.IdUsuario.Equals(Form1.idUserTurno) ? Form1.PartidaEnCurso.PJugador1 : Form1.PartidaEnCurso.PJugador2;
            if (ValidarCantidades(4, 1))
            {
                if (puntaje.Poker == 0)
                {
                    puntaje.Poker = 40;
                    return true;
                }
            }
            return false;
        }

        private bool EsFull()
        {
            Puntaje puntaje = Form1.PartidaEnCurso.Jugador1.IdUsuario.Equals(Form1.idUserTurno) ? Form1.PartidaEnCurso.PJugador1 : Form1.PartidaEnCurso.PJugador2;
            if (ValidarCantidades(3, 2))
            {
                if (puntaje.Full == 0)
                {
                    puntaje.Full = 30;
                    return true;
                }
            }
            return false;
        }

        private bool ValidarCantidades(int cant1, int cant2)
        {
            int i = 0;
            Dictionary<int, List<int>> acum = Form1.acumulador.GroupBy(r => r.Value).ToDictionary(t => i++, t => t.Select(r => r.Key).ToList());
            if (acum.Count > 2) return false;
            if (acum.GetValueOrDefault(0).Count.Equals(cant1) && acum.GetValueOrDefault(1).Count.Equals(cant2)) return true;
            if (acum.GetValueOrDefault(0).Count.Equals(cant2) && acum.GetValueOrDefault(1).Count.Equals(cant1)) return true;
            return false;
        }

        

        private bool EsGenerala()
        {
            int i = 0;
            Puntaje puntaje = Form1.PartidaEnCurso.Jugador1.IdUsuario.Equals(Form1.idUserTurno) ? Form1.PartidaEnCurso.PJugador1 : Form1.PartidaEnCurso.PJugador2;
            Dictionary<int, List<int>> acum = Form1.acumulador.GroupBy(r => r.Value).ToDictionary(t => i++, t => t.Select(r => r.Key).ToList());
            if (acum.Count == 1 && acum.GetValueOrDefault(0).Count == 5)
            {
                if (puntaje.Generala > 0)
                    puntaje.DoubleGenerala = 100;
                else if (puntaje.Generala == 0)
                    puntaje.Generala = 50;
                return true;
            }
            return false;
        }
    }
}
