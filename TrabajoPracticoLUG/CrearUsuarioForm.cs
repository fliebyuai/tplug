﻿using GeneralaController;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GeneralaViewLayer
{
    public partial class CrearUsuarioForm : Form
    {
        private UserController userController = new UserController();

        public CrearUsuarioForm()
        {
            InitializeComponent();
        }

        private void CrearUsuarioForm_Load(object sender, EventArgs e)
        {
            RefreshCombo();
        }

        private void RefreshCombo()
        {
            comboBox1.Hide();
            comboBox1.DataSource = null;
            comboBox1.DataSource = userController.findAll();
            comboBox1.DisplayMember = "Nombre";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                userController.CreateUser(textBoxUser.Text, textBoxPass.Text, textBoxName.Text, textBoxLast.Text);
                CleanFields();
                RefreshCombo();
            }
            catch (Exception error)
            {
                label5.Text = error.Message;
                return;
            }
        }

        private void CleanFields()
        {
            textBoxUser.Text = "";
            textBoxPass.Text = "";
            textBoxName.Text = "";
            textBoxLast.Text = "";
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
