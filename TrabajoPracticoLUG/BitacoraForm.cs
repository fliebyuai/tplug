﻿using GeneralaCommon;
using GeneralaController;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GeneralaViewLayer
{
    public partial class BitacoraForm : Form
    {
        UsuarioDTO usuario;
        BitacoraController bitacoraController = new BitacoraController();
        public BitacoraForm(UsuarioDTO usuario)
        {
            this.usuario = usuario;
            InitializeComponent();
        }

        private void BitacoraForm_Load(object sender, EventArgs e)
        {
            try
            {
                List<BitacoraDTO> bitacora = bitacoraController.getUserBitacora(usuario);
                dataGridView1.DataSource = bitacora;

            }
            catch (Exception errr)
            {
                errorBitacora.Text = errr.Message; 
                return;
            }
        }
    }
}
