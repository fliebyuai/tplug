﻿
namespace TrabajoPracticoLUG
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.promedioVictorias1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bitacora1 = new System.Windows.Forms.Button();
            this.logout1 = new System.Windows.Forms.Button();
            this.welcome1 = new System.Windows.Forms.Label();
            this.player1Login = new System.Windows.Forms.Button();
            this.player1PassTxt = new System.Windows.Forms.TextBox();
            this.player1UserNameTxt = new System.Windows.Forms.TextBox();
            this.player1ErrorLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tablaPuntajes = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bitacora2 = new System.Windows.Forms.Button();
            this.welcome2 = new System.Windows.Forms.Label();
            this.logout2 = new System.Windows.Forms.Button();
            this.player2Login = new System.Windows.Forms.Button();
            this.player2UsernameTxt = new System.Windows.Forms.TextBox();
            this.player2PassTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.player2ErrorLabel = new System.Windows.Forms.Label();
            this.iniciarPartida = new System.Windows.Forms.Button();
            this.finPartida = new System.Windows.Forms.Button();
            this.tirar1 = new System.Windows.Forms.Button();
            this.tirar2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.turno = new System.Windows.Forms.Label();
            this.informe1 = new System.Windows.Forms.Label();
            this.informe2 = new System.Windows.Forms.Label();
            this.partidasGanadas1 = new System.Windows.Forms.Label();
            this.partidasEmpatadas1 = new System.Windows.Forms.Label();
            this.partidasPerdidas1 = new System.Windows.Forms.Label();
            this.partidasPerdidas2 = new System.Windows.Forms.Label();
            this.partidasEmpatadas2 = new System.Windows.Forms.Label();
            this.partidasGanadas2 = new System.Windows.Forms.Label();
            this.partidasGanadas1Txt = new System.Windows.Forms.TextBox();
            this.partidasEmpatadas1txt = new System.Windows.Forms.TextBox();
            this.partidasPerdidas1txt = new System.Windows.Forms.TextBox();
            this.partidasPerdidas2txt = new System.Windows.Forms.TextBox();
            this.partidasEmpatadas2txt = new System.Windows.Forms.TextBox();
            this.partidasGanadas2Txt = new System.Windows.Forms.TextBox();
            this.promedioVictorias1txt = new System.Windows.Forms.TextBox();
            this.promedioVictorias2txt = new System.Windows.Forms.TextBox();
            this.promedioVictorias2 = new System.Windows.Forms.Label();
            this.tj2txt = new System.Windows.Forms.TextBox();
            this.tj2 = new System.Windows.Forms.Label();
            this.tj1txt = new System.Windows.Forms.TextBox();
            this.tj1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // promedioVictorias1
            // 
            this.promedioVictorias1.AutoSize = true;
            this.promedioVictorias1.Location = new System.Drawing.Point(51, 708);
            this.promedioVictorias1.Name = "promedioVictorias1";
            this.promedioVictorias1.Size = new System.Drawing.Size(184, 30);
            this.promedioVictorias1.TabIndex = 33;
            this.promedioVictorias1.Text = "Promedo victorias:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(271, 205);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "Registrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bitacora1);
            this.groupBox1.Controls.Add(this.logout1);
            this.groupBox1.Controls.Add(this.welcome1);
            this.groupBox1.Controls.Add(this.player1Login);
            this.groupBox1.Controls.Add(this.player1PassTxt);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.player1UserNameTxt);
            this.groupBox1.Controls.Add(this.player1ErrorLabel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(51, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(480, 328);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Jugador 1";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // bitacora1
            // 
            this.bitacora1.Location = new System.Drawing.Point(134, 205);
            this.bitacora1.Name = "bitacora1";
            this.bitacora1.Size = new System.Drawing.Size(266, 40);
            this.bitacora1.TabIndex = 3;
            this.bitacora1.Text = "Bitacora";
            this.bitacora1.UseVisualStyleBackColor = true;
            this.bitacora1.Click += new System.EventHandler(this.bitacora1_Click);
            // 
            // logout1
            // 
            this.logout1.Location = new System.Drawing.Point(134, 159);
            this.logout1.Name = "logout1";
            this.logout1.Size = new System.Drawing.Size(266, 40);
            this.logout1.TabIndex = 3;
            this.logout1.Text = "Cerrar Sesion";
            this.logout1.UseVisualStyleBackColor = true;
            this.logout1.Click += new System.EventHandler(this.logout1_Click);
            // 
            // welcome1
            // 
            this.welcome1.AutoSize = true;
            this.welcome1.Location = new System.Drawing.Point(80, 105);
            this.welcome1.Name = "welcome1";
            this.welcome1.Size = new System.Drawing.Size(0, 30);
            this.welcome1.TabIndex = 6;
            // 
            // player1Login
            // 
            this.player1Login.Location = new System.Drawing.Point(134, 205);
            this.player1Login.Name = "player1Login";
            this.player1Login.Size = new System.Drawing.Size(131, 40);
            this.player1Login.TabIndex = 5;
            this.player1Login.Text = "Iniciar";
            this.player1Login.UseVisualStyleBackColor = true;
            this.player1Login.Click += new System.EventHandler(this.player1Login_Click);
            // 
            // player1PassTxt
            // 
            this.player1PassTxt.Location = new System.Drawing.Point(134, 119);
            this.player1PassTxt.Name = "player1PassTxt";
            this.player1PassTxt.Size = new System.Drawing.Size(266, 35);
            this.player1PassTxt.TabIndex = 4;
            // 
            // player1UserNameTxt
            // 
            this.player1UserNameTxt.Location = new System.Drawing.Point(134, 70);
            this.player1UserNameTxt.Name = "player1UserNameTxt";
            this.player1UserNameTxt.Size = new System.Drawing.Size(266, 35);
            this.player1UserNameTxt.TabIndex = 3;
            // 
            // player1ErrorLabel
            // 
            this.player1ErrorLabel.AutoSize = true;
            this.player1ErrorLabel.Location = new System.Drawing.Point(40, 165);
            this.player1ErrorLabel.Name = "player1ErrorLabel";
            this.player1ErrorLabel.Size = new System.Drawing.Size(0, 30);
            this.player1ErrorLabel.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 30);
            this.label2.TabIndex = 1;
            this.label2.Text = "Clave: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usuario: ";
            // 
            // tablaPuntajes
            // 
            this.tablaPuntajes.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tablaPuntajes.ColumnCount = 13;
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.26968F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.744388F));
            this.tablaPuntajes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.542057F));
            this.tablaPuntajes.Location = new System.Drawing.Point(1199, 68);
            this.tablaPuntajes.Name = "tablaPuntajes";
            this.tablaPuntajes.RowCount = 3;
            this.tablaPuntajes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.75758F));
            this.tablaPuntajes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.24242F));
            this.tablaPuntajes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tablaPuntajes.Size = new System.Drawing.Size(978, 188);
            this.tablaPuntajes.TabIndex = 3;
            this.tablaPuntajes.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bitacora2);
            this.groupBox2.Controls.Add(this.welcome2);
            this.groupBox2.Controls.Add(this.logout2);
            this.groupBox2.Controls.Add(this.player2Login);
            this.groupBox2.Controls.Add(this.player2UsernameTxt);
            this.groupBox2.Controls.Add(this.player2PassTxt);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.player2ErrorLabel);
            this.groupBox2.Location = new System.Drawing.Point(565, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(464, 328);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Jugador 2";
            // 
            // bitacora2
            // 
            this.bitacora2.Location = new System.Drawing.Point(118, 205);
            this.bitacora2.Name = "bitacora2";
            this.bitacora2.Size = new System.Drawing.Size(266, 40);
            this.bitacora2.TabIndex = 7;
            this.bitacora2.Text = "Bitacora";
            this.bitacora2.UseVisualStyleBackColor = true;
            this.bitacora2.Click += new System.EventHandler(this.bitacora2_Click);
            // 
            // welcome2
            // 
            this.welcome2.AutoSize = true;
            this.welcome2.Location = new System.Drawing.Point(80, 105);
            this.welcome2.Name = "welcome2";
            this.welcome2.Size = new System.Drawing.Size(0, 30);
            this.welcome2.TabIndex = 7;
            // 
            // logout2
            // 
            this.logout2.Location = new System.Drawing.Point(118, 159);
            this.logout2.Name = "logout2";
            this.logout2.Size = new System.Drawing.Size(266, 40);
            this.logout2.TabIndex = 7;
            this.logout2.Text = "Cerrar Sesion";
            this.logout2.UseVisualStyleBackColor = true;
            this.logout2.Click += new System.EventHandler(this.logout2_Click);
            // 
            // player2Login
            // 
            this.player2Login.Location = new System.Drawing.Point(118, 205);
            this.player2Login.Name = "player2Login";
            this.player2Login.Size = new System.Drawing.Size(131, 40);
            this.player2Login.TabIndex = 12;
            this.player2Login.Text = "Iniciar";
            this.player2Login.UseVisualStyleBackColor = true;
            this.player2Login.Click += new System.EventHandler(this.player2Login_Click);
            // 
            // player2UsernameTxt
            // 
            this.player2UsernameTxt.Location = new System.Drawing.Point(118, 70);
            this.player2UsernameTxt.Name = "player2UsernameTxt";
            this.player2UsernameTxt.Size = new System.Drawing.Size(266, 35);
            this.player2UsernameTxt.TabIndex = 10;
            // 
            // player2PassTxt
            // 
            this.player2PassTxt.Location = new System.Drawing.Point(118, 119);
            this.player2PassTxt.Name = "player2PassTxt";
            this.player2PassTxt.Size = new System.Drawing.Size(266, 35);
            this.player2PassTxt.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 30);
            this.label5.TabIndex = 7;
            this.label5.Text = "Usuario: ";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(255, 205);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(129, 40);
            this.button3.TabIndex = 6;
            this.button3.Text = "Registrar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 30);
            this.label4.TabIndex = 8;
            this.label4.Text = "Clave: ";
            // 
            // player2ErrorLabel
            // 
            this.player2ErrorLabel.AutoSize = true;
            this.player2ErrorLabel.Location = new System.Drawing.Point(24, 165);
            this.player2ErrorLabel.Name = "player2ErrorLabel";
            this.player2ErrorLabel.Size = new System.Drawing.Size(0, 30);
            this.player2ErrorLabel.TabIndex = 9;
            // 
            // iniciarPartida
            // 
            this.iniciarPartida.Location = new System.Drawing.Point(322, 407);
            this.iniciarPartida.Name = "iniciarPartida";
            this.iniciarPartida.Size = new System.Drawing.Size(492, 40);
            this.iniciarPartida.TabIndex = 4;
            this.iniciarPartida.Text = "Iniciar Partida";
            this.iniciarPartida.UseVisualStyleBackColor = true;
            this.iniciarPartida.Click += new System.EventHandler(this.iniciarPartida_Click);
            // 
            // finPartida
            // 
            this.finPartida.Location = new System.Drawing.Point(1494, 12);
            this.finPartida.Name = "finPartida";
            this.finPartida.Size = new System.Drawing.Size(492, 40);
            this.finPartida.TabIndex = 5;
            this.finPartida.Text = "Terminar Partida";
            this.finPartida.UseVisualStyleBackColor = true;
            this.finPartida.Click += new System.EventHandler(this.finPartida_Click);
            // 
            // tirar1
            // 
            this.tirar1.BackColor = System.Drawing.SystemColors.Control;
            this.tirar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tirar1.Location = new System.Drawing.Point(1182, 1015);
            this.tirar1.Name = "tirar1";
            this.tirar1.Size = new System.Drawing.Size(173, 149);
            this.tirar1.TabIndex = 6;
            this.tirar1.Text = "Tirar";
            this.tirar1.UseVisualStyleBackColor = false;
            this.tirar1.Click += new System.EventHandler(this.tirar1_Click);
            // 
            // tirar2
            // 
            this.tirar2.Location = new System.Drawing.Point(2004, 1015);
            this.tirar2.Name = "tirar2";
            this.tirar2.Size = new System.Drawing.Size(173, 149);
            this.tirar2.TabIndex = 7;
            this.tirar2.Text = "Tirar";
            this.tirar2.UseVisualStyleBackColor = true;
            this.tirar2.Click += new System.EventHandler(this.tirar2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(1245, 589);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(143, 126);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(1436, 589);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(143, 126);
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(1623, 589);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(143, 126);
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(1799, 589);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(143, 126);
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(1974, 589);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(143, 126);
            this.pictureBox5.TabIndex = 12;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // pictureBox15
            // 
            this.pictureBox15.Location = new System.Drawing.Point(1872, 1036);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(84, 82);
            this.pictureBox15.TabIndex = 17;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Location = new System.Drawing.Point(1759, 1036);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(84, 82);
            this.pictureBox14.TabIndex = 16;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Location = new System.Drawing.Point(1642, 1036);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(84, 82);
            this.pictureBox13.TabIndex = 15;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Location = new System.Drawing.Point(1512, 1036);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(84, 82);
            this.pictureBox12.TabIndex = 14;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Location = new System.Drawing.Point(1386, 1036);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(84, 82);
            this.pictureBox11.TabIndex = 13;
            this.pictureBox11.TabStop = false;
            // 
            // turno
            // 
            this.turno.AutoSize = true;
            this.turno.Location = new System.Drawing.Point(1617, 1156);
            this.turno.Name = "turno";
            this.turno.Size = new System.Drawing.Size(0, 30);
            this.turno.TabIndex = 18;
            // 
            // informe1
            // 
            this.informe1.AutoSize = true;
            this.informe1.Font = new System.Drawing.Font("Segoe UI", 11.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.informe1.ForeColor = System.Drawing.Color.Red;
            this.informe1.Location = new System.Drawing.Point(1182, 960);
            this.informe1.Name = "informe1";
            this.informe1.Size = new System.Drawing.Size(0, 37);
            this.informe1.TabIndex = 19;
            // 
            // informe2
            // 
            this.informe2.AutoSize = true;
            this.informe2.Font = new System.Drawing.Font("Segoe UI", 11.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.informe2.ForeColor = System.Drawing.Color.Red;
            this.informe2.Location = new System.Drawing.Point(2007, 960);
            this.informe2.Name = "informe2";
            this.informe2.Size = new System.Drawing.Size(0, 37);
            this.informe2.TabIndex = 20;
            // 
            // partidasGanadas1
            // 
            this.partidasGanadas1.AutoSize = true;
            this.partidasGanadas1.Location = new System.Drawing.Point(51, 564);
            this.partidasGanadas1.Name = "partidasGanadas1";
            this.partidasGanadas1.Size = new System.Drawing.Size(175, 30);
            this.partidasGanadas1.TabIndex = 21;
            this.partidasGanadas1.Text = "Partidas ganadas:";
            // 
            // partidasEmpatadas1
            // 
            this.partidasEmpatadas1.AutoSize = true;
            this.partidasEmpatadas1.Location = new System.Drawing.Point(51, 611);
            this.partidasEmpatadas1.Name = "partidasEmpatadas1";
            this.partidasEmpatadas1.Size = new System.Drawing.Size(199, 30);
            this.partidasEmpatadas1.TabIndex = 22;
            this.partidasEmpatadas1.Text = "Partidas empatadas:";
            // 
            // partidasPerdidas1
            // 
            this.partidasPerdidas1.AutoSize = true;
            this.partidasPerdidas1.Location = new System.Drawing.Point(51, 661);
            this.partidasPerdidas1.Name = "partidasPerdidas1";
            this.partidasPerdidas1.Size = new System.Drawing.Size(176, 30);
            this.partidasPerdidas1.TabIndex = 23;
            this.partidasPerdidas1.Text = "Partidas perdidas:";
            // 
            // partidasPerdidas2
            // 
            this.partidasPerdidas2.AutoSize = true;
            this.partidasPerdidas2.Location = new System.Drawing.Point(565, 661);
            this.partidasPerdidas2.Name = "partidasPerdidas2";
            this.partidasPerdidas2.Size = new System.Drawing.Size(176, 30);
            this.partidasPerdidas2.TabIndex = 26;
            this.partidasPerdidas2.Text = "Partidas perdidas:";
            // 
            // partidasEmpatadas2
            // 
            this.partidasEmpatadas2.AutoSize = true;
            this.partidasEmpatadas2.Location = new System.Drawing.Point(565, 611);
            this.partidasEmpatadas2.Name = "partidasEmpatadas2";
            this.partidasEmpatadas2.Size = new System.Drawing.Size(199, 30);
            this.partidasEmpatadas2.TabIndex = 25;
            this.partidasEmpatadas2.Text = "Partidas empatadas:";
            // 
            // partidasGanadas2
            // 
            this.partidasGanadas2.AutoSize = true;
            this.partidasGanadas2.Location = new System.Drawing.Point(565, 564);
            this.partidasGanadas2.Name = "partidasGanadas2";
            this.partidasGanadas2.Size = new System.Drawing.Size(175, 30);
            this.partidasGanadas2.TabIndex = 24;
            this.partidasGanadas2.Text = "Partidas ganadas:";
            // 
            // partidasGanadas1Txt
            // 
            this.partidasGanadas1Txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.partidasGanadas1Txt.Location = new System.Drawing.Point(356, 561);
            this.partidasGanadas1Txt.Name = "partidasGanadas1Txt";
            this.partidasGanadas1Txt.Size = new System.Drawing.Size(175, 35);
            this.partidasGanadas1Txt.TabIndex = 27;
            // 
            // partidasEmpatadas1txt
            // 
            this.partidasEmpatadas1txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.partidasEmpatadas1txt.Location = new System.Drawing.Point(356, 611);
            this.partidasEmpatadas1txt.Name = "partidasEmpatadas1txt";
            this.partidasEmpatadas1txt.Size = new System.Drawing.Size(175, 35);
            this.partidasEmpatadas1txt.TabIndex = 28;
            // 
            // partidasPerdidas1txt
            // 
            this.partidasPerdidas1txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.partidasPerdidas1txt.Location = new System.Drawing.Point(356, 661);
            this.partidasPerdidas1txt.Name = "partidasPerdidas1txt";
            this.partidasPerdidas1txt.Size = new System.Drawing.Size(175, 35);
            this.partidasPerdidas1txt.TabIndex = 29;
            // 
            // partidasPerdidas2txt
            // 
            this.partidasPerdidas2txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.partidasPerdidas2txt.Location = new System.Drawing.Point(854, 659);
            this.partidasPerdidas2txt.Name = "partidasPerdidas2txt";
            this.partidasPerdidas2txt.Size = new System.Drawing.Size(175, 35);
            this.partidasPerdidas2txt.TabIndex = 32;
            // 
            // partidasEmpatadas2txt
            // 
            this.partidasEmpatadas2txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.partidasEmpatadas2txt.Location = new System.Drawing.Point(854, 609);
            this.partidasEmpatadas2txt.Name = "partidasEmpatadas2txt";
            this.partidasEmpatadas2txt.Size = new System.Drawing.Size(175, 35);
            this.partidasEmpatadas2txt.TabIndex = 31;
            // 
            // partidasGanadas2Txt
            // 
            this.partidasGanadas2Txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.partidasGanadas2Txt.Location = new System.Drawing.Point(854, 559);
            this.partidasGanadas2Txt.Name = "partidasGanadas2Txt";
            this.partidasGanadas2Txt.Size = new System.Drawing.Size(175, 35);
            this.partidasGanadas2Txt.TabIndex = 30;
            // 
            // promedioVictorias1txt
            // 
            this.promedioVictorias1txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.promedioVictorias1txt.Location = new System.Drawing.Point(356, 708);
            this.promedioVictorias1txt.Name = "promedioVictorias1txt";
            this.promedioVictorias1txt.Size = new System.Drawing.Size(175, 35);
            this.promedioVictorias1txt.TabIndex = 34;
            // 
            // promedioVictorias2txt
            // 
            this.promedioVictorias2txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.promedioVictorias2txt.Location = new System.Drawing.Point(854, 708);
            this.promedioVictorias2txt.Name = "promedioVictorias2txt";
            this.promedioVictorias2txt.Size = new System.Drawing.Size(175, 35);
            this.promedioVictorias2txt.TabIndex = 36;
            // 
            // promedioVictorias2
            // 
            this.promedioVictorias2.AutoSize = true;
            this.promedioVictorias2.Location = new System.Drawing.Point(565, 708);
            this.promedioVictorias2.Name = "promedioVictorias2";
            this.promedioVictorias2.Size = new System.Drawing.Size(192, 30);
            this.promedioVictorias2.TabIndex = 35;
            this.promedioVictorias2.Text = "Promedio Victorias:";
            // 
            // tj2txt
            // 
            this.tj2txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tj2txt.Location = new System.Drawing.Point(854, 749);
            this.tj2txt.Name = "tj2txt";
            this.tj2txt.Size = new System.Drawing.Size(175, 35);
            this.tj2txt.TabIndex = 40;
            // 
            // tj2
            // 
            this.tj2.AutoSize = true;
            this.tj2.Location = new System.Drawing.Point(565, 749);
            this.tj2.Name = "tj2";
            this.tj2.Size = new System.Drawing.Size(183, 30);
            this.tj2.TabIndex = 39;
            this.tj2.Text = "Tiempo de Juego: ";
            // 
            // tj1txt
            // 
            this.tj1txt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tj1txt.Location = new System.Drawing.Point(356, 749);
            this.tj1txt.Name = "tj1txt";
            this.tj1txt.Size = new System.Drawing.Size(175, 35);
            this.tj1txt.TabIndex = 38;
            // 
            // tj1
            // 
            this.tj1.AutoSize = true;
            this.tj1.Location = new System.Drawing.Point(51, 749);
            this.tj1.Name = "tj1";
            this.tj1.Size = new System.Drawing.Size(183, 30);
            this.tj1.TabIndex = 37;
            this.tj1.Text = "Tiempo de Juego: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(249, 30);
            this.label3.TabIndex = 41;
            this.label3.Text = "TRABAJO PRACTICO LUG";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(278, 30);
            this.label6.TabIndex = 42;
            this.label6.Text = "Docente: Christian Parkinson";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(490, 30);
            this.label7.TabIndex = 43;
            this.label7.Text = "Alumno: Facundo Lieby > facundolieby@gmail.com";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(51, 960);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(594, 175);
            this.panel1.TabIndex = 44;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2259, 1201);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tj2txt);
            this.Controls.Add(this.tj2);
            this.Controls.Add(this.tj1txt);
            this.Controls.Add(this.tj1);
            this.Controls.Add(this.promedioVictorias2txt);
            this.Controls.Add(this.promedioVictorias2);
            this.Controls.Add(this.promedioVictorias1txt);
            this.Controls.Add(this.promedioVictorias1);
            this.Controls.Add(this.partidasPerdidas2txt);
            this.Controls.Add(this.partidasEmpatadas2txt);
            this.Controls.Add(this.partidasGanadas2Txt);
            this.Controls.Add(this.partidasPerdidas1txt);
            this.Controls.Add(this.partidasEmpatadas1txt);
            this.Controls.Add(this.partidasGanadas1Txt);
            this.Controls.Add(this.partidasPerdidas2);
            this.Controls.Add(this.partidasEmpatadas2);
            this.Controls.Add(this.partidasGanadas2);
            this.Controls.Add(this.partidasPerdidas1);
            this.Controls.Add(this.partidasEmpatadas1);
            this.Controls.Add(this.partidasGanadas1);
            this.Controls.Add(this.informe2);
            this.Controls.Add(this.informe1);
            this.Controls.Add(this.turno);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tirar2);
            this.Controls.Add(this.tirar1);
            this.Controls.Add(this.finPartida);
            this.Controls.Add(this.iniciarPartida);
            this.Controls.Add(this.tablaPuntajes);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button player1Login;
        private System.Windows.Forms.TextBox player1PassTxt;
        private System.Windows.Forms.TextBox player1UserNameTxt;
        private System.Windows.Forms.Label player1ErrorLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button player2Login;
        private System.Windows.Forms.TextBox player2UsernameTxt;
        private System.Windows.Forms.TextBox player2PassTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label player2ErrorLabel;
        private System.Windows.Forms.Label welcome1;
        private System.Windows.Forms.Button logout1;
        private System.Windows.Forms.Button logout2;
        private System.Windows.Forms.Label welcome2;
        private System.Windows.Forms.Button bitacora1;
        private System.Windows.Forms.Button bitacora2;
        private System.Windows.Forms.TableLayoutPanel tablaPuntajes;
        private System.Windows.Forms.Button iniciarPartida;
        private System.Windows.Forms.Button finPartida;
        private System.Windows.Forms.Button tirar1;
        private System.Windows.Forms.Button tirar2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label turno;
        private System.Windows.Forms.Label informe1;
        private System.Windows.Forms.Label informe2;
        private System.Windows.Forms.Label partidasGanadas1;
        private System.Windows.Forms.Label partidasEmpatadas1;
        private System.Windows.Forms.Label partidasPerdidas1;
        private System.Windows.Forms.Label partidasPerdidas2;
        private System.Windows.Forms.Label partidasEmpatadas2;
        private System.Windows.Forms.Label partidasGanadas2;
        private System.Windows.Forms.TextBox partidasGanadas1Txt;
        private System.Windows.Forms.TextBox partidasEmpatadas1txt;
        private System.Windows.Forms.TextBox partidasPerdidas1txt;
        private System.Windows.Forms.TextBox partidasPerdidas2txt;
        private System.Windows.Forms.TextBox partidasEmpatadas2txt;
        private System.Windows.Forms.TextBox partidasGanadas2Txt;
        private System.Windows.Forms.TextBox promedioVictorias1txt;
        private System.Windows.Forms.Label promedioVictorias1;
        private System.Windows.Forms.TextBox promedioVictorias2txt;
        private System.Windows.Forms.Label promedioVictorias2;
        private System.Windows.Forms.TextBox tj2txt;
        private System.Windows.Forms.Label tj2;
        private System.Windows.Forms.TextBox tj1txt;
        private System.Windows.Forms.Label tj1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
    }
}

