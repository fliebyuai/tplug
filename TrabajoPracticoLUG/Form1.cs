﻿using GeneralaCommon;
using GeneralaController;
using GeneralaViewLayer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace TrabajoPracticoLUG
{
    public partial class Form1 : Form
    {

        CrearUsuarioForm crearUsuarioForm = new CrearUsuarioForm();
        BitacoraForm bitacoraForm;
        UserController userController = new UserController();
        PartidaController partidaController = new PartidaController();
        PartidaHelper partidaHelper;
        public static PartidaDTO PartidaEnCurso;
        public static UsuarioDTO jugador1;
        public static UsuarioDTO jugador2;
        public static int idUserTurno;
        public static Dictionary<int, int> acumulador = new Dictionary<int, int>();
        public static Dictionary<int, int> tiro = new Dictionary<int, int>();
        public static int countTiros = 0;


        public Form1()
        {
            InitializeComponent();
            partidaHelper = new PartidaHelper(pictureBox1, pictureBox2, pictureBox3, pictureBox4, pictureBox5, pictureBox11, pictureBox12, pictureBox13, pictureBox14, pictureBox15, informe1, informe2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Registracion();
        }

        private void Registracion()
        {
            crearUsuarioForm.Show();
            crearUsuarioForm.Focus();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GC.Collect();
            partidaHelper.PreloadResultsTable(tablaPuntajes);
            RefreshUsers();

        }

        
        
        

        private void button3_Click(object sender, EventArgs e)
        {
            Registracion();
        }

        private void player1Login_Click(object sender, EventArgs e)
        {
            try
            {
                jugador1 = Login(player1UserNameTxt.Text, player1PassTxt.Text);
                RefreshUsers();
            }
            catch (Exception err)
            {
                player1ErrorLabel.Text = err.Message;
                return;
            }
        }

        private void RefreshUsers()
        {
            if (jugador1 != null && jugador2 != null)
            {
                iniciarPartida.Show();
                hideShowPartidas(2, true);
                hideShowPartidas(1, true);
            }
            else
            {
                iniciarPartida.Show();
                hideShowPartidas(2, false);
                hideShowPartidas(1, false);
                finPartida.Hide();
                tablaPuntajes.Hide();
                iniciarPartida.Hide();
                tirar1.Hide();
                tirar2.Hide();
                pictureBox1?.Hide();
                pictureBox2?.Hide();
                pictureBox3?.Hide();
                pictureBox4?.Hide();
                pictureBox5?.Hide();
                if (pictureBox1!=null) pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                if (pictureBox2 != null) pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                if (pictureBox3 != null) pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
                if (pictureBox4 != null) pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
                if (pictureBox5 != null) pictureBox5.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox11?.Hide();
                pictureBox12?.Hide();
                pictureBox13?.Hide();
                pictureBox14?.Hide();
                pictureBox15?.Hide();
                if (pictureBox11 != null) pictureBox11.SizeMode = PictureBoxSizeMode.StretchImage;
                if (pictureBox12 != null) pictureBox12.SizeMode = PictureBoxSizeMode.StretchImage;
                if (pictureBox13 != null) pictureBox13.SizeMode = PictureBoxSizeMode.StretchImage;
                if (pictureBox14 != null) pictureBox14.SizeMode = PictureBoxSizeMode.StretchImage;
                if (pictureBox15 != null) pictureBox15.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            if (jugador1 != null)
            {
                //Esta logueado
                label1.Hide();
                label2.Hide();
                player1ErrorLabel.Hide();
                player1UserNameTxt.Hide();
                player1PassTxt.Hide();
                player1Login.Hide();
                button1.Hide();
                welcome1.Show();
                welcome1.Text = "Bienvenido " + jugador1.Nombre + " " + jugador1.Apellido;
                logout1.Show();
                bitacora1.Show();
                hideShowPartidas(1, true);
            }
            else
            {
                label1.Show();
                label2.Show();
                player1ErrorLabel.Show();
                player1UserNameTxt.Show();
                player1PassTxt.Show();
                player1Login.Show();
                button1.Show();
                welcome1.Hide();
                welcome1.Text = "";
                logout1.Hide();
                bitacora1.Hide();
                hideShowPartidas(1, false);
            }
            if (jugador2 != null)
            {
                //Esta logueado
                label5.Hide();
                label4.Hide();
                player2ErrorLabel.Hide();
                player2UsernameTxt.Hide();
                player2PassTxt.Hide();
                player2Login.Hide();
                button3.Hide();
                welcome2.Show();
                welcome2.Text = "Bienvenido " + jugador2.Nombre + " " + jugador2.Apellido;
                logout2.Show();
                bitacora2.Show();
                hideShowPartidas(2, true);
            }
            else
            {
                label5.Show();
                label4.Show();
                player2ErrorLabel.Show();
                player2UsernameTxt.Show();
                player2PassTxt.Show();
                player2Login.Show();
                button3.Show();
                welcome2.Hide();
                welcome2.Text = "";
                logout2.Hide();
                bitacora2.Hide();
                hideShowPartidas(2, false);
            }
            cargarEstadisticas();
        }

        private void cargarEstadisticas()
        {
            if(jugador1 != null)
            {
                EstadisticasDTO stats = partidaController.getStatistics(jugador1);
                if (stats != null)
                {
                    partidasEmpatadas1txt.Text = stats.PartidasEmpatadas.ToString();
                    partidasGanadas1Txt.Text = stats.PartidasGanadas.ToString();
                    partidasPerdidas1txt.Text = stats.PartidasPerdidas.ToString();
                    promedioVictorias1txt.Text = stats.PromedioVictorias.ToString() + " %";
                    tj1txt.Text = stats.TiempoDeJuego + " Min";
                }
            }
            if (jugador2 != null)
            {
                EstadisticasDTO stats = partidaController.getStatistics(jugador2);
                if (stats != null)
                {
                    partidasEmpatadas2txt.Text = stats.PartidasEmpatadas.ToString();
                    partidasGanadas2Txt.Text = stats.PartidasGanadas.ToString();
                    partidasPerdidas2txt.Text = stats.PartidasPerdidas.ToString();
                    promedioVictorias2txt.Text = stats.PromedioVictorias.ToString() + " %";
                    tj2txt.Text = stats.TiempoDeJuego + " Min";
                }
            }
            
        }

        private void hideShowPartidas(int user, bool show)
        {
            if(user == 1)
            {
                if(show)
                    partidasEmpatadas1.Show();
                else
                    partidasEmpatadas1.Hide();

                if (show)
                    partidasEmpatadas1txt.Show();
                else
                    partidasEmpatadas1txt.Hide();

                if (show)
                    partidasGanadas1.Show();
                else
                    partidasGanadas1.Hide();

                if (show)
                    partidasGanadas1Txt.Show();
                else
                    partidasGanadas1Txt.Hide();

                if (show)
                    partidasPerdidas1.Show();
                else
                    partidasPerdidas1.Hide();

                if (show)
                    partidasPerdidas1txt.Show();
                else
                    partidasPerdidas1txt.Hide();

                if (show)
                    promedioVictorias1.Show();
                else
                    promedioVictorias1.Hide();

                if (show)
                    promedioVictorias1txt.Show();
                else
                    promedioVictorias1txt.Hide();

                if (show)
                    tj1.Show();
                else
                    tj1.Hide();

                if (show)
                    tj1txt.Show();
                else
                    tj1txt.Hide();
            }
            if(user == 2)
            {
                if (show)
                    partidasEmpatadas2.Show();
                else
                    partidasEmpatadas2.Hide();

                if (show)
                    partidasEmpatadas2txt.Show();
                else
                    partidasEmpatadas2txt.Hide();

                if (show)
                    partidasGanadas2.Show();
                else
                    partidasGanadas2.Hide();

                if (show)
                    partidasGanadas2Txt.Show();
                else
                    partidasGanadas2Txt.Hide();

                if (show)
                    partidasPerdidas2.Show();
                else
                    partidasPerdidas2.Hide();

                if (show)
                    partidasPerdidas2txt.Show();
                else
                    partidasPerdidas2txt.Hide();

                if (show)
                    promedioVictorias2.Show();
                else
                    promedioVictorias2.Hide();

                if (show)
                    promedioVictorias2txt.Show();
                else
                    promedioVictorias2txt.Hide();

                if (show)
                    tj2.Show();
                else
                    tj2.Hide();

                if (show)
                    tj2txt.Show();
                else
                    tj2txt.Hide();
            }
        }

        private UsuarioDTO Login(string user, string pass)
        {
            if (jugador1 != null && jugador1.NombreUsuario.Equals(user))
            {
                throw new Exception("El usuario ya se encuentra logueado");
            }
            if (jugador2 != null && jugador2.NombreUsuario.Equals(user))
            {
                throw new Exception("El usuario ya se encuentra logueado");
            }
            return userController.login(user, pass);
        }

        private void player2Login_Click(object sender, EventArgs e)
        {
            try
            {
                jugador2 = Login(player2UsernameTxt.Text, player2PassTxt.Text);
                RefreshUsers();
            }
            catch (Exception err)
            {
                player2ErrorLabel.Text = err.Message;
                return;
            }
        }

        private void logout2_Click(object sender, EventArgs e)
        {
            logout(jugador2);
        }

        private void logout1_Click(object sender, EventArgs e)
        {
            logout(jugador1);
        }

        private void logout(UsuarioDTO jugador)
        {
            if (jugador1 != null && jugador1.NombreUsuario.Equals(jugador.NombreUsuario))
            {
                jugador1 = null;
            }
            if (jugador2 != null && jugador2.NombreUsuario.Equals(jugador.NombreUsuario))
            {
                jugador2 = null;
            }
            userController.logout(jugador);
            RefreshUsers();
        }

        private void bitacora1_Click(object sender, EventArgs e)
        {
            OpenBitacora(jugador1);
        }

        private void OpenBitacora(UsuarioDTO jugador)
        {
            bitacoraForm = new BitacoraForm(jugador);
            bitacoraForm.Show();
        }

        private void bitacora2_Click(object sender, EventArgs e)
        {
            OpenBitacora(jugador2);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void iniciarPartida_Click(object sender, EventArgs e)
        {

            groupBox1.Hide();
            groupBox2.Hide();
            RefreshUsers();
            finPartida.Show();
            iniciarPartida.Hide();
            tablaPuntajes.Show();
            IniciarPartida();
            hideShowPartidas(2, false);
            hideShowPartidas(1, false);
        }

        private void IniciarPartida()
        {
            PartidaEnCurso = new PartidaDTO(jugador1, jugador2);
            partidaController.iniciarPartida(PartidaEnCurso);
            partidaHelper.CompletarNombresJugadores(tablaPuntajes);
            tirar1.Show();
            tirar2.Show();
            pictureBox1.Show();
            pictureBox2.Show();
            pictureBox3.Show();
            pictureBox4.Show();
            pictureBox5.Show();
            idUserTurno = jugador1.IdUsuario;
            turno.Text = "Turno de " + jugador1.Nombre;
            partidaHelper.DibujarPuntos(tablaPuntajes);
            informe1.Text = "";
            informe2.Text = "";
        }

        

        private void TerminarPartida()
        {

            partidaController.terminarPartida(PartidaEnCurso);
            PartidaEnCurso = null;
            turno.Text = null;
        }

        private void finPartida_Click(object sender, EventArgs e)
        {
            
            groupBox1.Show();
            groupBox2.Show();
            RefreshUsers();
            finPartida.Hide();
            iniciarPartida.Show();
            tablaPuntajes.Hide();
            tirar1.Hide();
            tirar2.Hide();
            informe1.Hide();
            informe2.Hide();
            TerminarPartida();
            PartidaEnCurso = null;
            idUserTurno = 0;
            acumulador = new Dictionary<int, int>();
            tiro = new Dictionary<int, int>();
            countTiros = 0;
            pictureBox1.Hide();
            pictureBox2.Hide();
            pictureBox3.Hide();
            pictureBox4.Hide();
            pictureBox5.Hide();
            pictureBox11.Hide();
            pictureBox12.Hide();
            pictureBox13.Hide();
            pictureBox14.Hide();
            pictureBox15.Hide();
            pictureBox1.Image = null;
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            pictureBox4.Image = null;
            pictureBox5.Image = null;
            pictureBox11.Image = null;
            pictureBox12.Image = null;
            pictureBox13.Image = null;
            pictureBox14.Image = null;
            pictureBox15.Image = null;
        }

        private void finalizarPartida()
        {
            Puntaje ganador = Form1.PartidaEnCurso.PJugador1.getTotal() > Form1.PartidaEnCurso.PJugador2.getTotal() ? Form1.PartidaEnCurso.PJugador1 : Form1.PartidaEnCurso.PJugador1.getTotal() < Form1.PartidaEnCurso.PJugador2.getTotal() ? Form1.PartidaEnCurso.PJugador1 : null;
            if (ganador != null)
                MessageBox.Show("Felicitaciones jugador " + ganador.Jugador.Nombre + " " + ganador.Jugador.Apellido + " ha ganado el juego con " + ganador.getTotal() + " Puntos!!");
            else
                MessageBox.Show("Empate!!");
            groupBox1.Show();
            groupBox2.Show();
            RefreshUsers();
            finPartida.Hide();
            iniciarPartida.Show();
            tablaPuntajes.Hide();
            TerminarPartida();
        }

        private void tirar1_Click(object sender, EventArgs e)
        {
            if (!partidaHelper.FinDeJuego())
            {
                if (idUserTurno.Equals(jugador1.IdUsuario) && countTiros < 3)
                {
                    partidaHelper.tirar();
                    partidaHelper.RenderDados();
                    countTiros++;
                    partidaHelper.validarJugada(turno, tablaPuntajes);
                }
                else
                {
                    MessageBox.Show("No es su turno");
                }
            }
            else
            {
                finalizarPartida();
            }
            

        }

        private void tirar2_Click(object sender, EventArgs e)
        {
            if (!partidaHelper.FinDeJuego())
            {
                if (idUserTurno.Equals(jugador2.IdUsuario) && countTiros < 3)
                {
                    partidaHelper.tirar();
                    partidaHelper.RenderDados();
                    countTiros++;
                    partidaHelper.validarJugada(turno, tablaPuntajes);
                }
                else
                {
                    MessageBox.Show("No es su turno");
                }
            }
            else
            {
                finalizarPartida();
            }

        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            partidaHelper.clickPictureBox(pictureBox1, pictureBox11, 1, turno, tablaPuntajes);
        }

        

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            partidaHelper.clickPictureBox(pictureBox2, pictureBox12, 2, turno, tablaPuntajes);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            partidaHelper.clickPictureBox(pictureBox3, pictureBox13, 3, turno, tablaPuntajes);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            partidaHelper.clickPictureBox(pictureBox4, pictureBox14, 4, turno, tablaPuntajes);
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            partidaHelper.clickPictureBox(pictureBox5, pictureBox15, 5, turno, tablaPuntajes);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
