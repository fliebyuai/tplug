﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaModel
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public List<Partida> Partidas { get; set; }
        public List<Bitacora> Bitacoras { get; set; }
    }
}
