﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaModel
{
    public class Bitacora
    {
        public int IdBitacora { get; set; }
        public Usuario Usuario { get; set; }
        public string Tipo { get; set; }
        public DateTime Datetime { get; set; }

    }
}
