﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaModel
{
    public class Partida
    {
        public int IdPartida { get; set; }
        public Usuario Usuario1 { get; set; }
        public Usuario Usuario2 { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public int PuntajeUsuario1 { get; set; }
        public int PuntajeUsuario2 { get; set; }
    }
}
