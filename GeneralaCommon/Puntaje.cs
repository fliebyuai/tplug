﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaCommon
{
    public class Puntaje
    {
        public UsuarioDTO Jugador { get; set; }
        public int One { get; set; }
        public int Two { get; set; }
        public int Three { get; set; }
        public int Four { get; set; }
        public int Five { get; set; }
        public int Six { get; set; }
        public int Stair { get; set; }
        public int Full { get; set; }
        public int Poker { get; set; }
        public int Generala { get; set; }
        public int DoubleGenerala { get; set; }

        public int getTotal()
        {
            int _sum = 0;
            if (One >= 0) _sum += One;
            if (Two >= 0) _sum += Two;
            if (Three >= 0) _sum += Three;
            if (Four >= 0) _sum += Four;
            if (Five >= 0) _sum += Five;
            if (Six >= 0) _sum += Six;
            if (Stair >= 0) _sum += Stair;
            if (Full >= 0) _sum += Full;
            if (Poker >= 0) _sum += Poker;
            if (Generala >= 0) _sum += Generala;
            if (DoubleGenerala >= 0) _sum += DoubleGenerala;
            return _sum;
        }

    }
}
