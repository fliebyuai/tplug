﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaCommon
{
    public class UsuarioDTO
    {
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

    }
}
