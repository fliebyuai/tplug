﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaCommon
{
    public class PartidaDTO
    {
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public UsuarioDTO Jugador1 { get; set; }
        public UsuarioDTO Jugador2 { get; set; }
        public Puntaje PJugador1 { get; set; }
        public Puntaje PJugador2 { get; set; }

        public PartidaDTO() { }
        public PartidaDTO(UsuarioDTO jugador1, UsuarioDTO jugador2)
        {
            this.Jugador1 = jugador1;
            this.Jugador2 = jugador2;
            PJugador1 = new Puntaje();
            PJugador1.Jugador = jugador1;
            PJugador2 = new Puntaje();
            PJugador2.Jugador = jugador2;
            this.FechaInicio = DateTime.Now;
        }

        public int getPuntaje(UsuarioDTO dto)
        {
            Puntaje puntaje = new Puntaje();
            if (dto.IdUsuario.Equals(Jugador1.IdUsuario)) 
                puntaje = PJugador1;
            if (dto.IdUsuario.Equals(Jugador2.IdUsuario))
                puntaje = PJugador2;
            return puntaje.One + puntaje.Two + puntaje.Three + puntaje.Four + puntaje.Five + puntaje.Six + puntaje.Stair + puntaje.Full + puntaje.Poker + puntaje.Generala + puntaje.DoubleGenerala;
        }
    }
}
