﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaCommon
{
    public class BitacoraDTO
    {
        public string Tipo { get; set; }
        public DateTime Fecha { get; set; }
    }
}
