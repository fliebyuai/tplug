﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaCommon
{
    public class EstadisticasDTO
    {
        public int PartidasGanadas { get; set; }
        public int PartidasEmpatadas { get; set; }
        public int PartidasPerdidas { get; set; }

        public int PromedioVictorias { get; set; }

        public int TiempoDeJuego { get; set; }
    }
}
