﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace GeneralaDAL
{
    public class AccesoDB<T>
    {
        SqlConnection sqlConnection;
        T entity = (T)Activator.CreateInstance(typeof(T));

        public void OpenConnection()
        {
            sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = "Data Source=.;Initial Catalog=GENERALA;Integrated Security=True";
            sqlConnection.Open();
        }

        public void CloseConnection()
        {
            sqlConnection.Close();
            sqlConnection = null;
            GC.Collect();
        }

        private SqlCommand CreateCommand(string sqlCommand, List<SqlParameter> parameters = null)
        {
            SqlCommand command = new SqlCommand();
            command.Connection = sqlConnection;
            command.CommandType = CommandType.Text;
            command.CommandText = sqlCommand;
            if (parameters != null)
                command.Parameters.AddRange(parameters.ToArray());
            return command;
        }

        public SqlParameter CreateParam(string id, Object value)
        {
            SqlParameter sqlParam = new SqlParameter();
            sqlParam.ParameterName = "@" + id;
            sqlParam.Value = value;
            if (value is int)
                sqlParam.DbType = DbType.Int32;
            if (value is string)
                sqlParam.DbType = DbType.String;
            if (value is DateTime)
                sqlParam.DbType = DbType.DateTime;
            return sqlParam;

        }

        public DataTable ReadAll(List<SqlParameter> parameterss = null)
        {
            OpenConnection();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string select = "SELECT * FROM " + entity.GetType().ToString().Split('.')[1];
            adapter.SelectCommand = CreateCommand(select, parameterss);
            DataTable table = new DataTable();
            adapter.Fill(table);
            CloseConnection();
            return table;
        }

        public DataTable Read(string sqlWhere, List<SqlParameter> parameterss = null)
        {
            OpenConnection();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string select = "SELECT * FROM " + entity.GetType().ToString().Split('.')[1] + " WHERE " + sqlWhere;
            adapter.SelectCommand = CreateCommand(select, parameterss);
            DataTable table = new DataTable();
            adapter.Fill(table);
            CloseConnection();
            return table;
        }

        public int Insert(List<SqlParameter> parameters = null)
        {
            int result = 0;
            OpenConnection();
            string insert = "INSERT INTO " + entity.GetType().ToString().Split('.')[1] + " (" + ConcatParams(parameters, false) + ") VALUES (" + ConcatParams(parameters, true) + ")";
            SqlCommand command = CreateCommand(insert, parameters);
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                result = -1;
            }
            finally
            {
                CloseConnection();
            }
            return result;
        }

        public object Execute(string query, List<SqlParameter> parameters = null)
        {
            string result;
            OpenConnection();
            SqlCommand command = CreateCommand(query, parameters);
            try
            {
                result = command.ExecuteScalar().ToString();
            }
            catch (Exception e)
            {
                result = null;
            }
            finally
            {
                CloseConnection();
            }
            return result;
        }

        public int DeleteAll(List<SqlParameter> parameters = null)
        {
            int result = 0;
            OpenConnection();
            string delete = "DELETE FROM " + entity.GetType().ToString().Split('.')[1];
            SqlCommand command = CreateCommand(delete, parameters);
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                result = -1;
            }
            finally
            {
                CloseConnection();
            }
            return result;
        }

        public int Delete(string sqlWhere, List<SqlParameter> parameters = null)
        {
            int result = 0;
            OpenConnection();
            string delete = "DELETE FROM " + entity.GetType().ToString().Split('.')[1] + " WHERE " + sqlWhere;
            SqlCommand command = CreateCommand(delete, parameters);
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                result = -1;
            }
            finally
            {
                CloseConnection();
            }
            return result;
        }

        public int Edit(string sqlWhere, List<SqlParameter> parameters = null)
        {
            int result = 0;
            OpenConnection();
            string edit = "UPDATE " + entity.GetType().ToString().Split('.')[1] + " SET " + ConcatEdit(parameters) + " WHERE " + sqlWhere;
            SqlCommand command = CreateCommand(edit, parameters);
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                result = -1;
            }
            finally
            {
                CloseConnection();
            }
            return result;
        }

        private string ConcatEdit(List<SqlParameter> parameters)
        {
            string pmts = "";
            int i = 0;
            foreach (SqlParameter p in parameters)
            {
                pmts += p.ParameterName.Split('@')[1] + "=" + p.ParameterName;
                try
                {
                    if (parameters.ToArray()[i + 1] != null) pmts += ",";
                }
                catch (Exception)
                {
                    //nothing tbd
                }
                i++;
            }
            return pmts;
        }

        private string ConcatParams(List<SqlParameter> parameters, bool withAt)
        {
            string pmts = "";
            int i = 0;
            foreach (SqlParameter p in parameters)
            {
                if (withAt)
                    pmts += p.ParameterName;
                else
                    pmts += p.ParameterName.Split('@')[1];
                try
                {
                    if (parameters.ToArray()[i + 1] != null) pmts += ",";
                }
                catch (Exception)
                {
                    //nothing tbd
                }
                i++;
            }
            return pmts;
        }
    }
}
