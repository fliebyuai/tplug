﻿using GeneralaModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace GeneralaDAL
{
    public class UsuarioDAO : BaseDAO<Usuario>
    {

        //private PartidaDAO PartidaDAO = new PartidaDAO();
        //private BitacoraDAO BitacoraDAO = new BitacoraDAO();

        public override int Delete(Usuario entity)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(db.CreateParam("Id", entity.IdUsuario));
            return db.Delete("ID_USUARIO=@Id", parameters);
        }

        public override int Edit(Usuario entity)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(db.CreateParam("USUARIO", entity.NombreUsuario));
            parameters.Add(db.CreateParam("CLAVE", entity.Clave));
            parameters.Add(db.CreateParam("NOMBRE", entity.Nombre));
            parameters.Add(db.CreateParam("APELLIDO", entity.Apellido));
            return db.Edit("ID_USUARIO=" + entity.IdUsuario, parameters);
        }

        public Usuario findByUsername(string user)
        {
            DataTable dataTable = db.Read("USUARIO='" + user + "'");
            DataRow row = dataTable.Rows[0];

            Usuario usuario = new Usuario();
            usuario.IdUsuario = int.Parse(row["ID_USUARIO"].ToString());
            usuario.NombreUsuario = row["USUARIO"].ToString();
            usuario.Clave = row["CLAVE"].ToString();
            usuario.Nombre = row["NOMBRE"].ToString();
            usuario.Apellido = row["APELLIDO"].ToString();
            //usuario.Bitacoras = BitacoraDAO.FindAll();
            //usuario.Partidas = PartidaDAO.FindAll();

            return usuario;
        }

        public override List<Usuario> FindAll()
        {
            List<Usuario> usuarios = new List<Usuario>();
            DataTable dataTable = db.ReadAll();
            foreach(DataRow row in dataTable.Rows)
            {
                Usuario usuario = new Usuario();
                usuario.IdUsuario = int.Parse(row["ID_USUARIO"].ToString());
                usuario.NombreUsuario = row["USUARIO"].ToString();
                usuario.Clave = row["CLAVE"].ToString();
                usuario.Nombre = row["NOMBRE"].ToString();
                usuario.Apellido = row["APELLIDO"].ToString();
                //usuario.Bitacoras = BitacoraDAO.FindAll();
                //usuario.Partidas = PartidaDAO.FindAll();
                usuarios.Add(usuario);
            }
            return usuarios;
        }

        public override Usuario FindOne(object id)
        {
            DataTable dataTable = db.Read("ID_USUARIO="+id);
            DataRow row = dataTable.Rows[0];
            
            Usuario usuario = new Usuario();
            usuario.IdUsuario = int.Parse(row["ID_USUARIO"].ToString());
            usuario.NombreUsuario = row["USUARIO"].ToString();
            usuario.Clave = row["CLAVE"].ToString();
            usuario.Nombre = row["NOMBRE"].ToString();
            usuario.Apellido = row["APELLIDO"].ToString();
            //usuario.Bitacoras = BitacoraDAO.FindAll();
            //usuario.Partidas = PartidaDAO.FindAll();
            
            return usuario;
        }

        public override int Save(Usuario entity)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(db.CreateParam("USUARIO", entity.NombreUsuario));
            parameters.Add(db.CreateParam("CLAVE", entity.Clave));
            parameters.Add(db.CreateParam("NOMBRE", entity.Nombre));
            parameters.Add(db.CreateParam("APELLIDO", entity.Apellido));
            return db.Insert(parameters);
        }
    }
}
