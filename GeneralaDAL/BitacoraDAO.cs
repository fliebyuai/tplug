﻿using GeneralaModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace GeneralaDAL
{
    public class BitacoraDAO : BaseDAO<Bitacora>
    {
        private UsuarioDAO UsuarioDAO = new UsuarioDAO();
        public override int Delete(Bitacora entity)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(db.CreateParam("Id", entity.IdBitacora));
            return db.Delete("ID_BITACORA=@Id", parameters);
        }

        public override int Edit(Bitacora entity)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(db.CreateParam("ID_USUARIO", entity.Usuario.IdUsuario));
            parameters.Add(db.CreateParam("TIPO", entity.Tipo));
            parameters.Add(db.CreateParam("DATETIME", entity.Datetime));
            return db.Edit("ID_BITACORA=" + entity.IdBitacora, parameters);
        }

        public override List<Bitacora> FindAll()
        {
            List<Bitacora> bitacoras = new List<Bitacora>();
            DataTable dataTable = db.ReadAll();
            foreach (DataRow row in dataTable.Rows)
            {
                Bitacora bitacora = new Bitacora();
                bitacora.IdBitacora = int.Parse(row["ID_BITACORA"].ToString());
                bitacora.Usuario = UsuarioDAO.FindOne(int.Parse(row["ID_USUARIO"].ToString()));
                bitacora.Tipo = row["TIPO"].ToString();
                bitacora.Datetime = DateTime.Parse(row["DATETIME"].ToString());
                bitacoras.Add(bitacora);
            }
            return bitacoras;
        }

        public override Bitacora FindOne(object id)
        {
            DataTable dataTable = db.Read("ID_BITACORA="+id);
            DataRow row = dataTable.Rows[0];
            
            Bitacora bitacora = new Bitacora();
            bitacora.IdBitacora = int.Parse(row["ID_BITACORA"].ToString());
            bitacora.Usuario = UsuarioDAO.FindOne(int.Parse(row["ID_USUARIO"].ToString()));
            bitacora.Tipo = row["TIPO"].ToString();
            bitacora.Datetime = DateTime.Parse(row["DATETIME"].ToString());
            
            return bitacora;
        }

        public List<Bitacora> findByUser(int idUsuario)
        {
            List<Bitacora> bitacoras = new List<Bitacora>();
            DataTable dataTable = db.Read("ID_USUARIO="+idUsuario);
            foreach (DataRow row in dataTable.Rows)
            {
                Bitacora bitacora = new Bitacora();
                bitacora.IdBitacora = int.Parse(row["ID_BITACORA"].ToString());
                bitacora.Usuario = UsuarioDAO.FindOne(int.Parse(row["ID_USUARIO"].ToString()));
                bitacora.Tipo = row["TIPO"].ToString();
                bitacora.Datetime = DateTime.Parse(row["DATETIME"].ToString());
                bitacoras.Add(bitacora);
            }
            return bitacoras;
        }

        public override int Save(Bitacora entity)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(db.CreateParam("ID_USUARIO", entity.Usuario.IdUsuario));
            parameters.Add(db.CreateParam("TIPO", entity.Tipo));
            parameters.Add(db.CreateParam("DATETIME", entity.Datetime));
            return db.Insert(parameters);
        }
    }
}
