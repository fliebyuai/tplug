﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using GeneralaCommon;
using GeneralaModel;


namespace GeneralaDAL
{
    public class PartidaDAO : BaseDAO<Partida>
    {
        private UsuarioDAO UsuarioDAO = new UsuarioDAO();
        public override int Delete(Partida entity)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(db.CreateParam("Id", entity.IdPartida));
            return db.Delete("ID_PARTIDA=@Id", parameters);
        }

        public override int Edit(Partida entity)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(db.CreateParam("ID_USUARIO_1", entity.Usuario1.IdUsuario));
            parameters.Add(db.CreateParam("ID_USUARIO_2", entity.Usuario2.IdUsuario));
            parameters.Add(db.CreateParam("DATETIME_INICIO", entity.FechaInicio));
            parameters.Add(db.CreateParam("DATETIME_FIN", entity.FechaFin));
            parameters.Add(db.CreateParam("PUNTAJE_USUARIO1", entity.PuntajeUsuario1));
            parameters.Add(db.CreateParam("PUNTAJE_USUARIO2", entity.PuntajeUsuario2));
            return db.Edit("ID_PARTIDA=" + entity.IdPartida, parameters);
        }

        public override List<Partida> FindAll()
        {
            List<Partida> partidas = new List<Partida>();
            DataTable dataTable = db.ReadAll();
            foreach (DataRow row in dataTable.Rows)
            {
                Partida partida = new Partida();
                partida.IdPartida = int.Parse(row["ID_PARTIDA"].ToString());
                partida.Usuario1 = UsuarioDAO.FindOne(int.Parse(row["ID_USUARIO_1"].ToString()));
                partida.Usuario2 = UsuarioDAO.FindOne(int.Parse(row["ID_USUARIO_2"].ToString()));
                partida.FechaInicio = DateTime.Parse(row["DATETIME_INICIO"].ToString());
                partida.FechaFin = DateTime.Parse(row["DATETIME_FIN"].ToString());
                partida.PuntajeUsuario1 = int.Parse(row["PUNTAJE_USUARIO1"].ToString());
                partida.PuntajeUsuario2 = int.Parse(row["PUNTAJE_USUARIO2"].ToString());
                partidas.Add(partida);
            }
            return partidas;
        }

        public override Partida FindOne(object id)
        {
            DataTable dataTable = db.ReadAll();
            DataRow row = dataTable.Rows[0];
            
            Partida partida = new Partida();
            partida.IdPartida = int.Parse(row["ID_PARTIDA"].ToString());
            partida.Usuario1 = UsuarioDAO.FindOne(int.Parse(row["ID_USUARIO_1"].ToString()));
            partida.Usuario2 = UsuarioDAO.FindOne(int.Parse(row["ID_USUARIO_2"].ToString()));
            partida.FechaInicio = DateTime.Parse(row["DATETIME_INICIO"].ToString());
            partida.FechaFin = DateTime.Parse(row["DATETIME_FIN"].ToString());
            partida.PuntajeUsuario1 = int.Parse(row["PUNTAJE_USUARIO1"].ToString());
            partida.PuntajeUsuario2 = int.Parse(row["PUNTAJE_USUARIO2"].ToString());
            return partida;
        }

        public override int Save(Partida entity)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(db.CreateParam("ID_USUARIO_1", entity.Usuario1.IdUsuario));
            parameters.Add(db.CreateParam("ID_USUARIO_2", entity.Usuario2.IdUsuario));
            parameters.Add(db.CreateParam("DATETIME_INICIO", entity.FechaInicio));
            parameters.Add(db.CreateParam("DATETIME_FIN", entity.FechaFin));
            parameters.Add(db.CreateParam("PUNTAJE_USUARIO1", entity.PuntajeUsuario1));
            parameters.Add(db.CreateParam("PUNTAJE_USUARIO2", entity.PuntajeUsuario2));
            return db.Insert(parameters);
        }

        public int encontrarPartidasGanadas(string user)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter p = new SqlParameter();
            p.ParameterName = "@ID_USUARIO";
            p.Value = user;
            param.Add(p);
            string val = db.Execute("SELECT COUNT(*) FROM PARTIDA WHERE (ID_USUARIO_1=@ID_USUARIO AND PUNTAJE_USUARIO1>PUNTAJE_USUARIO2 ) OR (ID_USUARIO_2=@ID_USUARIO AND PUNTAJE_USUARIO2>PUNTAJE_USUARIO1 )",param).ToString();
            return int.Parse(val);
        }

        public int encontrarPartidasPerdidas(string user)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter p = new SqlParameter();
            p.ParameterName = "@ID_USUARIO";
            p.Value = user;
            param.Add(p);
            string val = db.Execute("SELECT COUNT(*) FROM PARTIDA WHERE (ID_USUARIO_1=@ID_USUARIO AND PUNTAJE_USUARIO1<PUNTAJE_USUARIO2 ) OR (ID_USUARIO_2=@ID_USUARIO AND PUNTAJE_USUARIO2<PUNTAJE_USUARIO1 )", param).ToString();
            return int.Parse(val);
        }

        public int encontrarPartidasEmpatadas(string user)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter p = new SqlParameter();
            p.ParameterName = "@ID_USUARIO";
            p.Value = user;
            param.Add(p);
            string val = db.Execute("SELECT COUNT(*) FROM PARTIDA WHERE (ID_USUARIO_1=@ID_USUARIO AND PUNTAJE_USUARIO1=PUNTAJE_USUARIO2 ) OR (ID_USUARIO_2=@ID_USUARIO AND PUNTAJE_USUARIO2=PUNTAJE_USUARIO1 )", param).ToString();
            return int.Parse(val);
        }

        public int buscarTiempodeJuego(string user)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter p = new SqlParameter();
            p.ParameterName = "@ID_USUARIO";
            p.Value = user;
            param.Add(p);
            string val = db.Execute("SELECT SUM(DATEDIFF(minute, DATETIME_INICIO, DATETIME_FIN)) FROM PARTIDA WHERE ID_USUARIO_1 = @ID_USUARIO OR ID_USUARIO_2 = @ID_USUARIO", param).ToString();
            return int.Parse(val);
        }
    }
}
