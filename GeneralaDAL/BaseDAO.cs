﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneralaDAL
{
    public abstract class BaseDAO<T>
    {
        protected AccesoDB<T> db = new AccesoDB<T>();

        public abstract int Save(T entity);
        public abstract int Delete(T entity);

        public abstract int Edit(T entity);

        public abstract List<T> FindAll();

        public abstract T FindOne(Object id);
    }
}
